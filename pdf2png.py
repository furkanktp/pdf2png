from argparse import ArgumentParser
from pathlib import Path
from pdf2image import convert_from_path


def convert_pdf_to_png(pdf_path):
    """Converts a pdf to a png."""
    path = Path(pdf_path)
    file_name = path.stem
    convert_from_path(
        pdf_path,
        dpi=300,
        fmt="png",
        transparent=False,
        output_folder=path.parent,
        output_file=file_name,
    )
    print(f"Conversion successful: {path.parent}/{file_name}.png")


if __name__ == "__main__":
    parser = ArgumentParser(description="Converts pdf to png.")
    parser.add_argument("pdf_path", help="pdf file path", type=str)

    args = parser.parse_args()
    if Path(args.pdf_path).is_file():
        convert_pdf_to_png(args.pdf_path)
    elif Path(args.pdf_path).is_dir():
        for artwork_path in Path(args.pdf_path).glob("*.pdf"):
            convert_pdf_to_png(str(artwork_path))
