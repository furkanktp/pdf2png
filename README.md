# pdf2png

This script converts PDF files to PNG images, using `pdf2image` python library.
It will save the image in the same directory as the PDF file.

## Installation
Installation guide for pdf2image: https://github.com/Belval/pdf2image

Start by cloning the repository, and installing the requirements:

```bash
git clone https://gitlab.com/furkanktp/pdf2png.git
cd pdf2png
pip install -r requirements.txt
```

## Usage

```bash
python -m pdf2png input.pdf
```
This command will create `input.png` in the same directory as `input.pdf`.
